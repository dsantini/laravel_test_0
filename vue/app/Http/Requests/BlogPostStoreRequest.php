<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Inizializzato con: php artisan make:request BlogPostStoreRequest
 */
class BlogPostStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "titolo" => ["required", "string", "min:5", "max:30", "bail"],
            "contenuto" => ["required", "string", "max:1000"]
        ];
    }
}
