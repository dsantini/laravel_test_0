<?php

namespace App\Repositories;

use App\Models\BlogPost;
use Illuminate\Database\Eloquent\Collection;
use Cache;

/**
 * ! Classe inutile
 * ! Utile solo come esempio per illustrare la dependency injection
 */
class BlogPostRepository
{
    /**
     * @return Collection<BlogPost>
     */
    public function all()
    {
        //return BlogPost::all();
        return Cache::remember("posts", 60, function () {
            return BlogPost::all();
        });
    }
}
