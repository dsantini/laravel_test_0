<?php

namespace App\Traits;

trait ColumnCompletenessCheckable
{
    public function areColumnsComplete()
    {
        return count($this->getFillable()) == count($this->getAttributes());
    }
}
