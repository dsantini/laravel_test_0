<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\BlogPostController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\DependencyInjectionController;

Route::get("/text", function () {
    return "wololooo";
});

Route::get("/test", function () {
    $a = "pippo";
    $b = "pluto";
    return view("test", ["x" => $a, "y" => $b]);
});

Route::get("/dumptest", function () {
    // dd() : https://laravel.com/docs/8.x/helpers#method-dd
    dd(["a" => "b", "c" => "Route test dd (dump)"]);
});

// https://laravel.com/docs/8.x/routing#parameters-and-dependency-injection
// http://127.0.0.1:8000/param/56/val/44?z=5
Route::get("/param/{x}/val/{y}", function (Request $req, $x, $y) {
    // dd() : https://laravel.com/docs/8.x/helpers#method-dd
    dd(["x" => $x, "y" => $y, "z" => $req->get("z"), "url" => $req->fullUrl(), "IP" => $req->ip(), "params" => $req->all()]);
});

Route::get("/testSelect", [BlogPostController::class, "testSelect"]);

Route::get("/testAutore", [BlogPostController::class, "testAutore"]);

Route::get("/testPostAutore", [BlogPostController::class, "testPostAutore"]);

Route::get("/post", [BlogPostController::class, "index"]);
Route::post("/post", [BlogPostController::class, "create"]);
Route::delete("/post", [BlogPostController::class, "destroy"]);
Route::put("/post", [BlogPostController::class, "update"]);

Route::get("/depInjTest", [DependencyInjectionController::class, "test"]);

Route::get("/tagWithPost", [TagController::class, "collegatiAUnPost"]);
