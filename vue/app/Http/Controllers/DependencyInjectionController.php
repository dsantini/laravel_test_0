<?php

namespace App\Http\Controllers;

use App\Models\BlogPost;
use Illuminate\Database\Eloquent\Collection;
use App\Repositories\BlogPostRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DependencyInjectionController extends Controller
{
    /**
     * @param Request $req
     * @param BlogPostRepository $repo
     * @return Response
     */
    public function test(Request $req, BlogPostRepository $repo)
    {
        return response(
            $repo->all()->map(function ($p) {
                return $p->titolo;
            })->reduce(function ($x, $y) {
                return "$x <br> $y";
            }, "")
        );
    }
}
