<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Password resettata.',
    'sent' => 'Abbiamo inviato una mail con il link per il reset della password!',
    'throttled' => 'Aspetta prima di riprovare.',
    'token' => 'Token errato.',
    'user' => "Non esiste un utente con questo indirizzo mail.",

];
