<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Controller;
use App\Repositories\BlogPostRepository;

/**
 * Inizializzato con: php artisan make:provider BlogPostProvider
 */
class BlogPostProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app
            ->when(Controller::class)
            ->needs(BlogPostRepository::class)
            ->give(function () {
 // give() o singleton()
                return new BlogPostRepository();
            });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
