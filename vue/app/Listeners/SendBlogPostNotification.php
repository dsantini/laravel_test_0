<?php

namespace App\Listeners;

use App\Events\BlogPostCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\BlogPost;
use App\Mail\BlogPostMail;
use Mail;

/**
 * Inizializzato con: php artisan event:generate
 * https://laravel.com/docs/8.x/events
 */
class SendBlogPostNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogPostCreated  $event
     * @return void
     */
    public function handle(BlogPostCreated $event)
    {
        Mail::to("pippo@pluto.com")->send(new BlogPostMail($event->blogPost));
    }
}
