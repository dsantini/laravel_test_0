<?php

namespace App\Http\Controllers;

use App\Models\BlogPost;
use App\Http\Requests\BlogPostStoreRequest;
use App\Mail\BlogPostMail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\Collection;
use Exception;
use Log;
use Mail;
use Event;
use App\Events\BlogPostCreated;
use App\Jobs\SendBlogReport;
use App\Http\Resources\BlogPostResource;

class BlogPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response|BlogPostResource
     */
    public function index(Request $request)
    {
        // https://laravel.com/docs/8.x/logging
        Log::emergency("emergency");
        Log::alert("alert");
        Log::critical("critical");
        Log::error("error");
        Log::warning("warning");
        Log::notice("notice");
        Log::info("info");
        Log::debug("debug");

        //Log::log(LOG_INFO, "log");

        Log::channel("canale_bello")->info("info");

        $posts = BlogPost::get();

        /*dd($posts->map(function (BlogPost $post) {
            return [$post, $post->tags];
        }));*/

        if ($request->is("api/*")) {
            $out = new BlogPostResource($posts);
        } else {
            $out = response(
                $posts->map(function (BlogPost $p) {
                    return $p->titolo;
                })->reduce(function (string $x, string $y) {
                    return "$x <br> $y";
                }, "<h1>Post esistenti</h1>")
            );
        }
        return $out;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        throw new Exception("Not implemented");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\BlogPostStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogPostStoreRequest $request)
    {
        $blogPost = new BlogPost();

        //Mail::to($request->user())->send(new BlogPostMail($blogPost));
        Event::dispatch(new BlogPostCreated($blogPost));
        SendBlogReport::dispatch();

        throw new Exception("Not implemented");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BlogPost  $blogPost
     * @return \Illuminate\Http\Response
     */
    public function show(BlogPost $blogPost)
    {
        throw new Exception("Not implemented");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BlogPost  $blogPost
     * @return \Illuminate\Http\Response
     */
    public function edit(BlogPost $blogPost)
    {
        throw new Exception("Not implemented");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BlogPost  $blogPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BlogPost $blogPost)
    {
        throw new Exception("Not implemented");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BlogPost  $blogPost
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlogPost $blogPost)
    {
        if ($blogPost->delete()) {
            $ret = response(["success" => true, "message" => "Post eliminato"]);
        } else {
            $ret = response(["success" => false, "message" => "Eliminazione fallita"], 500);
        }
        return $ret;
    }

    /**
     * Get the author of a blog post
     *
     * //@return \Illuminate\Http\Response
     * @return Response|void
     */
    public function testAutore(Request $req)
    {
        $blogPost = BlogPost::where("id", $req->get("id"))->first();
        if ($blogPost) {
            dd($blogPost->autore()->first());
        } else {
            return response("Nessun post presente con questo ID");
        }
    }

    /**
     * Get other posts of the same author of a blog post
     *
     * @return Response|void
     */
    public function testPostAutore(Request $req)
    {
        $id = $req->get("id");
        $blogPost = BlogPost::where("id", $id)->first();
        if ($blogPost) {
            $autore = $blogPost->autore()->first();
            if ($autore) {
                $out = $autore->posts()->where("id", "!=", $id)->get()->all();
            } else {
                $out = "Nessun autore associato a questo post";
            }
        } else {
            $out = "Nessun post presente con questo ID";
        }
        dd($out);
    }

    /**
     * Elenco dei titoli che iniziano in "A" e relativi sottotitoli
     *
     * @see https://laravel.com/docs/8.x/queries
     * @see https://laravel.com/docs/8.x/collections
     *
     * @return void
     */
    public function testSelect()
    {
        $blogPosts = BlogPost::getTitoliA()->map(function (BlogPost $p) {
            return [
                "Post" => $p,
                "Completo" => $p->areColumnsComplete(),
                "Contenuto" => $p->contenuto
            ];
        })->all();
        dd($blogPosts);
    }
}
