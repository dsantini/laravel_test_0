<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Tag extends Model
{
    use HasFactory;

    protected $table = "tag";

    protected $fillable = ['nome' => 'string'];

    /**
     * @return BelongsToMany<BlogPost>
     */
    public function posts()
    {
        return $this->belongsToMany("App\Models\BlogPost", "r_post_tag", "tag_id", "post_id");
    }
}
