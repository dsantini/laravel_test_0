# Laravel

## Documentazione

**https://laravel.com/docs/8.x**

### Installazione

1. `composer global require "laravel/installer:^4.2"`
2. `composer create-project laravel/laravel pippo-project`

Docs: https://laravel.com/docs/8.x/installation

### Avvio istanza locale per anteprima in tempo reale

- `php artisan serve`
- Docs: https://laravel.com/docs/8.x/artisan

### Eloquent ORM

- `php artisan make:model Pippo`
- https://laravel.com/docs/8.x/eloquent
- https://laravel.com/docs/8.x/eloquent-relationships
- https://laravel.com/docs/8.x/eloquent-collections

#### Controller

- `php artisan make:controller PippoController --model=Pippo`
- Docs: https://laravel.com/docs/8.x/controllers

#### Factory

- https://laravel.com/docs/8.x/database-testing#defining-model-factories

#### Seeder

- `php artisan make:seeder PippoSeeder`
- https://laravel.com/docs/8.x/seeding

#### Migration

1. Creare la migration per creare la tabella DB
    - `php artisan make:migration create_pippo_table --create=pippo`
    - Docs: https://laravel.com/docs/8.x/migrations
2. Implementare la migration
3. Eseguire la migration (eventualmente partendo da zero o eseguendo il seeder)
    - `php artisan migrate`
    - `php artisan migrate:fresh`
    - `php artisan migrate --seed`

#### Creazione completa veloce

- `php artisan make:model Pippo --all`
- Produce model, migration, factory, seeder e controller
- Docs: https://laravel.com/docs/8.x/eloquent#generating-model-classes

#### API Resources

- `php artisan make:resource PippoResource`
- Docs: https://laravel.com/docs/8.x/eloquent-resources

### Request

- `php artisan make:request PippoRequest`
- Docs: https://laravel.com/docs/8.x/validation#form-request-validation

### Starter Kit

1. Installare Laravel Breeze
    - `composer require laravel/breeze --dev`
    - Docs: https://laravel.com/docs/8.x/starter-kits
2. Installare uno degli starter kit
    - Vanilla: `php artisan breeze:install`
    - Vue: `php artisan breeze:install vue`
    - React: `php artisan breeze:install react`
3. `npm install`
4. `npm run dev`
5. `php artisan migrate`
6. `php artisan serve`

### Mail

- `php artisan make:mail PippoMail`
- Docs: https://laravel.com/docs/8.x/mail

### Jobs

- `php artisan make:job PippoJob`
- Docs: https://laravel.com/docs/8.x/queues

### Comandi console

- `php artisan make:command PippoCommand`
- Docs: https://laravel.com/docs/8.x/artisan#writing-commands

#### Scheduling
Simil-crontab

- `php artisan schedule:list`
- Docs: https://laravel.com/docs/8.x/scheduling

### Service providers

- `php artisan make:provider PippoProvider`
- Docs: https://laravel.com/docs/8.x/providers

### Gestione cache

- `php artisan cache:clear`
- `php artisan view:cache`
- `php artisan view:clear`
- `php artisan route:cache`
- `php artisan route:clear`

### Testing
#### Feature Test e Unit Test

1. Creare un Feature Test o Unit Test
    - `php artisan make:test PippoTest`
    - `php artisan make:test PippoTest --unit`
2. Implementare il test
3. Eseguire i test
    - `php artisan test`
    - `php artisan test --coverage-html=coverage_html`

- https://laravel.com/docs/8.x/testing
- https://laravel.com/docs/8.x/http-tests
- https://laravel.com/docs/8.x/database-testing

#### Browser Test

1. `composer require --dev laravel/dusk`
2. `php artisan dusk:install`
3. `php artisan dusk:make PippoTest`
4. Implementare i test
5. Eseguire i test
    - `php artisan dusk`
    - `php artisan dusk --group=foo`

- https://laravel.com/docs/8.x/dusk

#### Mocking

- https://laravel.com/docs/8.x/mocking
- https://laravel.com/docs/7.x/database-testing#writing-factories
- https://fakerphp.github.io/

### Rigenerazione application key

Salvata in `.env`, rigenerazione necessaria solo se si perde la precedente.

- `php artisan key:generate`
- Docs: https://laravel.com/docs/8.x/encryption

### Broadcasting

- https://laravel.com/docs/8.x/broadcasting

## Plugin

### Laravel DebugBar

- `composer require barryvdh/laravel-debugbar --dev`
- Docs: https://github.com/barryvdh/laravel-debugbar

### Psalm plugin for Laravel

1. `composer require --dev vimeo/psalm`
2. `./vendor/bin/psalm --init`
3. `composer require --dev psalm/plugin-laravel`
4. `./vendor/bin/psalm-plugin enable psalm/plugin-laravel`

Docs: https://packagist.org/packages/psalm/plugin-laravel

### Eloquent Model Generator

1. `composer require --dev krlove/eloquent-model-generator`
2. Usare il plugin, eventualmente specificando la tabella
    - `php artisan krlove:generate:model Pippo`
    - `php artisan krlove:generate:model Pippo --table-name=pippo`

Docs: https://github.com/krlove/eloquent-model-generator


## Quick start Vue

```sh
$NOME_PROGETTO='pippo'

composer global require "laravel/installer:^4.2"
# Crea progetto
composer create-project laravel/laravel $NOME_PROGETTO
cd $NOME_PROGETTO
# Install plugin 
composer require laravel/breeze barryvdh/laravel-debugbar vimeo/psalm --dev
./vendor/bin/psalm --init
composer require --dev psalm/plugin-laravel
./vendor/bin/psalm-plugin enable psalm/plugin-laravel
# Install Vue Starter Kit
php artisan breeze:install vue
npm install
npm run dev
php artisan migrate
php artisan serve
```
