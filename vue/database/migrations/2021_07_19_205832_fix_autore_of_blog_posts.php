<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * php artisan make:migration fix_autore_of_blog_posts
 * https://laravel.com/docs/8.x/migrations#foreign-key-constraints
 * https://stackoverflow.com/questions/22615926/migration-cannot-add-foreign-key-constraint
 */
class FixAutoreOfBlogPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blog_post', function (Blueprint $table) {
            $table->foreign('autore')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog_post', function (Blueprint $table) {
            $table->dropForeign(['autore']);
        });
    }
}
