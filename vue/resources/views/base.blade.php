<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <style>
        #container {
            background-color: blanchedalmond;
            text-align: center;
            margin: 15em;
            padding: 5em
        }
    </style>
</head>
<body>
    <div id="container">
        @yield('contenuto')
    </div>
</body>
</html>
