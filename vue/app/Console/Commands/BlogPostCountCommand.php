<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\BlogPost;

/**
 * Inizializzato con: php artisan make:command BlogPostCountCommand
 *
 * Esempi del comando:
 * php artisan blog:count --startsWith=A
 * php artisan blog:count
 */
class BlogPostCountCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blog:count {--startsWith=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Conta i post nel blog';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $startsWith = $this->option("startsWith");

        if ($startsWith) {
            $postCount = BlogPost::where('titolo', 'like', $startsWith . "%")->count();
        } else {
            $postCount = BlogPost::count();
        }

        if ($postCount == 1) {
            $out = "Un solo post trovato";
        } else {
            $out = "$postCount post trovati";
        }

        $this->line($out);
        $this->info($out);

        return 0;
    }
}
