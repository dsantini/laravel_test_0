<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Collection;
use App\Models\User;
use App\Models\Tag;
use App\Traits\ColumnCompletenessCheckable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Inizializzato con: php artisan make:model BlogPost
 */
class BlogPost extends Model
{
    use HasFactory;
    use ColumnCompletenessCheckable;
    use SoftDeletes;

    protected $table = "blog_post";

    /**
     * Campi modificabili globalmente
     *
     * @var array<string>
     */
    protected $fillable = [
        "titolo" => "string",
        "sottotitolo" => "string",
        "contenuto" => "string"
    ];

    /**
     * Autore del post
     *
     * @return BelongsTo<User>
     */
    public function autore()
    {
        return $this->belongsTo('App\Models\User', 'autore', 'id');
    }

    /**
     * @return BelongsToMany<Tag>
     */
    public function tags()
    {
        return $this->belongsToMany("App\Models\Tag", "r_post_tag", "post_id", "tag_id");
    }

    /**
     * Restituisce un elenco di titoli e sottotitoli di post il cui titolo inizia con A
     *
     * //@return array<BlogPost>
     * @return Collection<BlogPost>
     */
    public static function getTitoliA()
    {
        return static::select(["titolo", "sottotitolo"])->where("titolo", "like", "A%")->get();
    }
}

/*
 * Passi successivi:
 * php artisan make:migration create_blog_post_table
 * php artisan make:controller BlogPostController --model=BlogPost
 */
